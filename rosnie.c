#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include "libconway.h"

#define ZYWA(x, y) a[x][y] = ZYWA_KOMORKA

extern void run(int liczba_rund);
extern void start(int szer, int wys, char **tablica);

void zainicjalizuj(char **a)
{
	ZYWA(20, 20);
	ZYWA(20, 21);
	ZYWA(20, 24);
	ZYWA(21, 20);
	ZYWA(21, 23);
	ZYWA(22, 20);
	ZYWA(22, 23);
	ZYWA(22, 24);
	ZYWA(23, 22);
	ZYWA(24, 20);
	ZYWA(24, 22);
	ZYWA(24, 23);
	ZYWA(24, 24);
}

int main(void)
{
	int szer = 40;
	int wys = 30;
	char **a = zaalokuj(wys, szer);
	struct timespec odstep = {0};
	odstep.tv_nsec = 100000000;
	wypisz(szer, wys, a);
	zainicjalizuj(a);
	start(szer, wys, a);
	while (true) {
		run(1);
		wypisz(szer, wys, a);
		nanosleep(&odstep, NULL);
	}
	zwolnij(szer, a);
	return EXIT_SUCCESS;
}
