xzsection .bss
	tury    RESD    1
	plansza	RESQ	1
	x	RESD	1
	y	RESD	1
	sens	RESB	1 ;zmienna boolowska czy jest sens dzialac z
			  ;run, czyli x > 0, y > 0, plansza != null


section .text
global start, run
; Oznaczenia stosowane w P/Nieparzystych turach w przejsciu
	; Z(ywa)/M(artwa)->Z/M
	P_ZZ EQU BYTE 'O'
	P_MM EQU BYTE '.'
	P_ZM EQU BYTE ','
	P_MZ EQU BYTE 'o'
	N_ZZ EQU BYTE 'U'
	N_MM EQU BYTE 'Y'
	N_ZM EQU BYTE 'u'
	N_MZ EQU BYTE 'y'

	BEDZIE_ZYWA_BYLA_ZYWA1	EQU	3
	BEDZIE_ZYWA_BYLA_ZYWA2	EQU	2
	BEDZIE_ZYWA_BYLA_MARTWA	EQU	3

; void start(int szer, int wys, char T[][])
start:
	mov [sens], BYTE 1
	mov [x], edi
	mov [y], esi
	mov [plansza], rdx
	cmp rdx, 0
	je .niemasensu
	cmp edi, 0
	jle .niemasensu
	cmp esi, 0
	jle .niemasensu
	mov [tury], DWORD 0
.koniec:
	ret
.niemasensu:
	mov [sens], BYTE 0
	jmp .koniec

; Ustawia pamiec tak, zeby w r12 byl rzadek za komorka, ktora bedziemy
; obrabiac, w R13 rzadek z komorkami, gdzie teraz obrabiamy,
; R14 rzadek przed nami, do patrzenia do przodu
%macro 	ustaw_pamieci_R12R13R14 0 ;w ecx jest x jaki nas obchodzi
	cmp ecx, 0
	je %%pierwszyraz	;r12 bedzie nieprzydatne
	mov r12, r13
	mov r13, r14
	add ecx, 1
	cmp ecx, [x]
	jge %%wyjdz_i_odejmij_ecx ;tylko jeden rzadek,r14 bezuzyteczne
	mov r14, [plansza]
	shl rcx, 3	;do r14 trzeba wsadzic plansza[rcx + 1]
	add r14, rcx
	shr rcx, 3
	add rcx, -1		;rcx nam sie przyda potem
	mov r14, [r14]
	jmp %%koniec
%%pierwszyraz:
	mov r13, [plansza]	;jak pierwszy raz to recznie zapakuj
	cmp [x], DWORD 1	;wszystko, wpp korzysta z tego co
	je %%bezr14		;wczesniej zostalo tam wrzucone
	mov r14, [r13 + 8]
	mov r13, [r13]
	jmp %%koniec
%%bezr14:			;co ma sie dziac jak [x] = 1
	mov r13, [r13]
	jmp %%koniec
%%wyjdz_i_odejmij_ecx:
	add ecx, -1
	jmp %%koniec
%%koniec:
%endmacro

; %1 - ile dodac do x
; %2 - ile dodac do y
; %3 - x komorki, ktorej sasiada liczymy
; %4 - y komorki, ktorej sasiada liczymy
; %5 - %8 - jaka komorka jest zywa
; w r10 i r11 sa [x], [y] - zeby szybciej sie liczylo
; w r12, r13, r14 sa rzadki z komorkami, nie ma sie co
; martwic jak w ktoryms jest lewa pamiec a nie powinien tam
; siegnac
; uzywam r15 do liczenia
; preprocesorowe ify sluza do tego, zeby nie sprawdzac, czy
; wspolrzedne nie sa poza zakresem jesli wiemy, ze nie sa, np
; x + 1 zawsze jest nieujemne, x - 1 zawsze bedzie mniejsze od
; szerokosci planszy, bo x jest w zbiorze [0, szerokosc]
%macro	dodaj_jesli_zywe 8
	mov r15, %3
	add r15, %1	;sprawdzanie, czy komorka jest w
;zakresie
%if %1 = -1
	cmp r15, 0	;czy x nie jest ujemny
	jl %%koniec
%endif
%if %1 = 1
	cmp r15d, r10d	;czy nie jest wieksze niz szerokosc
	jge %%koniec				;planszy
%endif
	mov r15, %4
	add r15, %2	;sprawdzanie, czy y nie jest wieksze
;niz wysokosc planszy
	cmp r15d, r11d
	jge %%koniec
	cmp r15d, 0	;sprawdzanie, czy y nie jest ujemne
	jl %%koniec
%if %1 = 0
;Jest w zakresie, teraz wrzucamy
	mov r15b, [r13 + %4 + %2]
;do r15b odpowiednia komorke, ktora
;bedzie potem sprawdzana czy jest zywa
%elif %1 = -1
	mov r15b, [r12 + %4 + %2]
%elif %1 = 1
	mov r15b, [r14 + %4 + %2]
%endif

	cmp r15b, %5	;Sprawdz, czy w r15 siedzi zywa
	je %%zwieksz	;komorka i zwieksz licznik jak trzeba
	cmp r15b, %6
	je %%zwieksz
	cmp r15b, %7
	je %%zwieksz
	cmp r15b, %8
	jne %%koniec
%%zwieksz:
	add al,	1
%%koniec:
%endmacro


; pierwszy argumet - x, ktorego sasiadow liczymy
; drugi argument - y, ktorego sasiadow liczymy
; trzeci, czwarty argument - jakie oznaczenie to zywa komorka
; uzywa ax, r10-r15
%macro 	policz_zywe 6
	mov al, 0
	dodaj_jesli_zywe -1, -1, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe -1, 0, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe -1, 1, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe 0, -1, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe 0, 1, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe 1, -1, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe 1, 0, %1, %2, %3, %4, %5, %6
	dodaj_jesli_zywe 1, 1, %1, %2, %3, %4, %5, %6
%endmacro

; pierwszy argumet - x, ktorego sasiadow liczymy
; drugi argument - y, ktorego sasiadow liczymy
; trzeci, czwarty argument - jakie oznaczenie to zywa komorka
; %5 - zywa -> zywa
; %6 - martwa -> zywa
; %7 - martwa -> martwa
; %8 - zywa -> martwa
; w tym makrze nie warto zmieniac r12-14, bo beda uzywane
; zaraz znowu w policz_zywe
%macro obrob 8
	mov r10, [x]
	mov r11, [y]
	policz_zywe %1, %2, %3, %4, %5, %8
	mov r15, r13
	add r15, %2
	cmp [r15], %3
	je %%bylazywa
	cmp [r15], %4
	je %%bylazywa
%%martwa:			;komorka obecnie jest martwa
	cmp al, BEDZIE_ZYWA_BYLA_MARTWA
	jne %%dalej_martwa
	mov [r15], %6
	jmp %%koniec
%%dalej_martwa:
	mov [r15], %7
	jmp %%koniec
%%bylazywa:
	cmp al, BEDZIE_ZYWA_BYLA_ZYWA1
	je %%przezywa
	cmp al, BEDZIE_ZYWA_BYLA_ZYWA2
	je %%przezywa
	mov [r15], %8
	jmp %%koniec
%%przezywa:
	mov [r15], %5
%%koniec:
%endmacro

; void run(int ile_krokow)
run:
	cmp [sens], BYTE 0
	je .ret
	cmp edi, 0
	je .ret;
	push r10
	push r11
	push r12		;zachowac rejestry, ktore trzeba
	push r13
	push r14
	push r15
	test [tury], DWORD 1	;pilnuje, z ktorymi stalymi
; powinnismy zaczac liczenie - jesli dotychczas zasymulowalismy
; parzysta liczbe tur to od poczatku, jak nie to od srodka petli
	jnz .jakparzyste
; teraz iterujemy, bedziemy jechac po nastepujacych
; iteratorach: w rcx jest iteracja po x, w rdx po y
; w edi rundy
.zliczaj:
	mov rcx, 0
.pox:
	ustaw_pamieci_R12R13R14
	mov rdx, 0
.poy:
	obrob rcx, rdx,                                  \
	      BYTE P_MZ, BYTE P_ZZ,                      \
	      BYTE N_ZZ, BYTE N_MZ, BYTE N_MM, BYTE N_ZM
	add edx, 1
	cmp edx, [y]
	jl .poy
	add ecx, 1
	cmp ecx, [x]
	jl .pox
	add edi, -1
	add [tury], DWORD 1
	cmp edi, 0
	je .koniec
; stosuje inne oznaczenia dla parzystych i dla nieparzystych rund
; wiec ciele petli sa dwa bardzo podobne zestawy instrukcji tylko
; z innymi stalymi.
.jakparzyste:
	mov rcx, 0
.pox2:
	ustaw_pamieci_R12R13R14
	mov rdx, 0
.poy2:
	obrob rcx, rdx,                                   \
	      BYTE N_MZ, BYTE N_ZZ,                       \
	      BYTE P_ZZ, BYTE P_MZ, BYTE P_MM, BYTE P_ZM
	add edx, 1
	cmp edx, [y]
	jl .poy2
	add ecx, 1
	cmp ecx, [x]
	jl .pox2
	add [tury], DWORD 1
	add edi, -1
	cmp edi, 0
	jne .zliczaj
.koniec:
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
.ret:
	ret
