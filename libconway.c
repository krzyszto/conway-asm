#include "libconway.h"
#include <stdlib.h>
#include <string.h>
#define _GNU_SOURCE
#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
const char ZYWA_KOMORKA = 'O';
const char MARTWA_KOMORKA = '.';

static char przetworz(const char komorka)
{
	char *znaleziono = strchr("OoUy", komorka);
	if (znaleziono == NULL)
		return MARTWA_KOMORKA;
	return ZYWA_KOMORKA;
}

void wypisz(const int szer, const int wys, char *const *const T)
{
	for (int i = 0; i < wys; ++i) {
		for (int j = 0; j < szer; ++j) {
			printf("%c", przetworz(T[j][i]));
		}
		printf("\n");
	}
}

void zwolnij(const int szer, char **T)
{
	for (int i = 0; i < szer; ++i) {
		free(T[i]);
	}
	free(T);
}

char** zaalokuj(const int dl, const int szer)
{
	char **wynik = malloc(szer * sizeof(char*));
	for (int i = 0; i < szer; ++i) {
		wynik[i] = malloc(sizeof(char) * dl);
	}
	for (int i = 0; i < szer; ++i) {
		for (int j = 0; j < dl; ++j) {
			wynik[i][j] = MARTWA_KOMORKA;
		}
	}
	return wynik;
}

char **wczytaj_stdin(int *x, int *y)
{
	char **wczytana_plansza;
	scanf("%i %i\n", x, y);
	wczytana_plansza = zaalokuj(*y, *x);
	for (int i = 0; i < *y; ++i) {
		for (int j = 0; j < *x ; ++j) {
			scanf("%c", j[wczytana_plansza] + i);
		}
		scanf("\n");
	}
	return wczytana_plansza;
}
