#include "libconway.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

extern void start(int szer, int wys, char **T);
extern void run(int ile_krokow);

int main()
{
	int x, y;
	char **plansza = wczytaj_stdin(&x, &y);
	struct timespec odstep = {.tv_nsec = 100000000};
	wypisz(x, y, plansza);
	start(x, y, plansza);
	while (true) {
		run(1);
		printf("\n");
		wypisz(x,  y,  plansza);
		nanosleep( & odstep, NULL);
	}
	return EXIT_SUCCESS;
}
