	section .text
	global malok, free, memcpy, nastos, zestosu

	;; wklada wszystkie sensowne rejestry na stos i wraca pod RAX
nastos:
	push 	RCX
	push 	RDX
	push 	RBX
	push 	RBP
	push 	RSI
	push 	RDI
	jmp 	RAX

	;; sciaga wszystkie sensowne rejestry ze stosu i wraca pod RAX
	;; funkcja z grubsza odwrotna od nastos
	;; (z dokladnoscia do RAX)
zestosu:
	pop 	RDI
	pop 	RSI
	pop 	RBP
	pop 	RBX
	pop 	RDX
	pop 	RCX
	jmp 	RAX

	;; Funkcja bierze w RAXie ile chcemy miejsca
	;; i daje w RAXie adres pamieci gdzie jest tyle zarezerwowane
	;; albo przerywa wykonanie programu.
	;; wraca pod RSI
malok:
	push 	RDX
	mov	RDX, RAX
	mov

	;; Zwalnia tyle miejsca ile jest RAX i wraca pod RDI
free:



	;; kopiuje z adresu pod RDI do adresu RAX, RDX bajtow
	;; i wraca pod RSI
memcpy:
