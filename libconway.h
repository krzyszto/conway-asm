#ifndef LIB_CONWAY_H
#define LIB_CONWAY_H

extern const char ZYWA_KOMORKA;
extern const char MARTWA_KOMORKA;

extern void wypisz(const int szer, const int wys,
		   char *const *const T);

extern void zwolnij(const int szer, char **T);

extern char** zaalokuj(const int wys, const int szer);

/* Wczytuje plansze ze standardowego wejścia. */
/* Funkcja zaklada poprawnosc danych wejsciowych. */
/* Format danych wejsciowych jak poniżej: */
/* 10 4 */
/* ..OOO..O.O */
/* ..O...0... */
/* .OO.O..O.O */
extern char** wczytaj_stdin(int *x, int *y);

#endif
