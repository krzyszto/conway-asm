CFLAGS=-Wall -Wextra -std=gnu11 -g
LDFLAGS=-Wall -Wextra -g
LIBS=libconway.o conway-asm.o
CC=gcc

all: glider-gun rosnie ogolne

glider-gun: $(LIBS) glider-gun.o
	$(CC) -o $@ $^ $(LDFLAGS)

ogolne: $(LIBS) ogolne.o
	$(CC) -o $@ $^ $(LDFLAGS)

rosnie: $(LIBS) rosnie.o
	$(CC) -o $@ $^ $(LDFLAGS)

conway-asm.o: kp332534-64bit.asm
	nasm -f elf64 $< -o $@ -g -F DWARF

clean:
	rm -f *.o *~ glider-gun rosnie ogolne
