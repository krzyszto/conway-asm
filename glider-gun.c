#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include "libconway.h"

extern void start(int szer, int wys, char **T);
extern void run(int ile_krokow);
#define ZYWA(x, y) a[x][y] = ZYWA_KOMORKA

void zainicjalizuj(char **a)
{
	ZYWA(0, 4);
	ZYWA(0, 5);
	ZYWA(1, 4);
	ZYWA(1, 5);
	ZYWA(10, 4);
	ZYWA(10, 5);
	ZYWA(10, 6);
	ZYWA(11, 3);
	ZYWA(11, 7);
	ZYWA(12, 2);
	ZYWA(12, 8);
	ZYWA(13, 8);
	ZYWA(13, 2);
	ZYWA(14, 5);
	ZYWA(15, 7);
	ZYWA(15, 3);
	ZYWA(16, 4);
	ZYWA(16, 5);
	ZYWA(16, 6);
	ZYWA(17, 5);
	ZYWA(20, 2);
	ZYWA(20, 3);
	ZYWA(20, 4);
	ZYWA(21, 2);
	ZYWA(21, 3);
	ZYWA(21, 4);
	ZYWA(22, 1);
	ZYWA(22, 5);
	ZYWA(24, 0);
	ZYWA(24, 1);
	ZYWA(24, 5);
	ZYWA(24, 6);
	ZYWA(34, 2);
	ZYWA(34, 3);
	ZYWA(35, 2);
	ZYWA(35, 3);
}

int main(void)
{
	int szer = 40;
	int wys = 30;
	char **a = zaalokuj(wys, szer);
	struct timespec odstep = {0};
	odstep.tv_nsec = 100000000;
	wypisz(szer, wys, a);
	zainicjalizuj(a);
	start(szer, wys, a);
	while (true) {
		run(1);
		wypisz(szer, wys, a);
		nanosleep(&odstep, NULL);
	}
	zwolnij(szer, a);
	return EXIT_SUCCESS;
}
